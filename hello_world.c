#include <stdio.h>
#include <assert.h>

#define MAX_STRING_LEN 64

int main(void)
{
    printf("Your name, please: ");
    char buf[MAX_STRING_LEN] = {0};
    assert(MAX_STRING_LEN == 64);
    scanf("%63s", buf); //< one char for \0
    printf("Hello world from %s!\n", buf);
    return 0;
}
